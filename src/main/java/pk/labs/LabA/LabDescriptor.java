package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Impl.Ekran.EkranBean";
    public static String controlPanelImplClassName = "pk.labs.LabA.Impl.PanelKontrolny.PanelKontrolnyBean";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.Programator";
    public static String mainComponentImplClassName = "pk.labs.LabA.Impl.Mikrofala.MikrofalaBean";
    // endregion

    // region P2
    public static String mainComponentBeanName = "mikro";
    public static String mainFrameBeanName = "main-frame";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly-frame";
    // endregion
}
